﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolStorage : MonoBehaviour {

    [SerializeField]
    List<GameObject> poolObjs;

	// Use this for initialization
	void Awake () {

        foreach(GameObject obj in poolObjs)
        {
            ObjectPool.CreatePoolObj(obj, 0);
        }
		
	}
	
}
