﻿using UnityEngine;
using System.Collections.Generic;


public static class ObjectPool {

    static GameObject poolParent;//the parent that all objects in the objectpool will be stored under

    static Dictionary<string, List<GameObject>> objPool = new Dictionary<string, List<GameObject>>();//string key is the name of one of the stored gameobjects 

    public static GameObject GetPoolObj(string requestedObj)
    {
        GameObject foundObj = objPool["Default"][0];//dummy default value

        if(requestedObj == null)
        {
            return foundObj;
        }

        if (objPool.ContainsKey(requestedObj) == true)//if the key is valid
        {          

            if (objPool[requestedObj].Count <= 2)
            {
                CreatePoolObj(objPool[requestedObj][0], 0);
            }

            foundObj = objPool[requestedObj][1];//get the second object in the list (preserve the first object to create a new object

            objPool[requestedObj].Remove(foundObj);//remove the object that will be returned from the list

            foundObj.transform.parent = null;//remove the objectpool parent for the object

            foundObj.SetActive(true);

        }
        else//if the key does not exist
        {
            Debug.Log("Invalid Key: " + requestedObj);
        }

        

        return foundObj;
    }

    public static void CreatePoolObj(GameObject obj, int warmCount)
    {
        if (objPool.ContainsKey("Default") == false) // perform initial setup if there is no default object in the dictonary SHOULD ONLY BE CALLED ONCE
        {
            poolParent = new GameObject("Object Pool");//create a new empty gameobject to serve as a parent for all other objects

            GameObject defaultObj = new GameObject("Default");//create an empty default gameobject
            defaultObj.transform.parent = poolParent.transform;//parent the default obj to the poolparent

            defaultObj.SetActive(false);
            objPool.Add("Default", new List<GameObject>() { defaultObj });//add an entry to the object pool
        }



        if (objPool.ContainsKey(obj.name) == true)//if the pool already contains this object then create a new copy of that object
        {

            GameObject newObj = GameObject.Instantiate(objPool[obj.name][0]);//create a copy of the original requested gameobject
            newObj.name = objPool[obj.name][0].name;//change the name of the new gameobject to the name of the orginal obect (removes the "(Clone)" from the name for when the object is returned

            newObj.transform.parent = poolParent.transform;

            newObj.SetActive(false);

            objPool[newObj.name].Add(newObj);//add the object to the pool dictonary


        }
        else//if the objectpool does not contain the given object then create the object and store it in the dictonary
        {
            GameObject originalObj = GameObject.Instantiate(obj);
            originalObj.name = obj.name;
            originalObj.SetActive(false);

            originalObj.transform.parent = poolParent.transform;

            objPool.Add(obj.name, new List<GameObject>() { originalObj });

            //Debug.Log("Created Pool Obj: " + obj.name);
        }


        WarmPoolObj(obj.name, warmCount);
        
        
        
        
    }

    public static void ReturnPoolObj(GameObject _toReturn)
    {
        _toReturn.SetActive(false);//deactivate the object

        _toReturn.transform.parent = poolParent.transform;//child the object to the object pool parent

        objPool[_toReturn.name].Add(_toReturn);//add the object back into the pool dictonary

    }

    public static void WarmPoolObj(string objId, int count)//create pool objects until you reach the warm count
    {

        for(int objsWarmed = 0; objsWarmed < count; objsWarmed++)
        {
            CreatePoolObj(objPool[objId][0], 0);
        }
    }

    public static bool CheckForPoolObj(string objId)//check if the objpool contains a given object
    {
        return objPool.ContainsKey(objId);
    }
}
