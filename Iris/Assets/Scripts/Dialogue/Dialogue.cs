﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dialogue {

    public string leftExpressionKey;

    public string rightExpressionKey;

    public string speakingChar;

    public string text;

    public string audioID;

    public string speakingSide;

    public Dialogue(string __leftKey, string __rightKey, string _speaking, string _speakingSide, string _text, string _audioID)
    {

        leftExpressionKey = __leftKey;

        rightExpressionKey = __rightKey;

        speakingChar = _speaking;

        text = _text;

        audioID = _audioID;

        speakingSide = _speakingSide;

    }
}
