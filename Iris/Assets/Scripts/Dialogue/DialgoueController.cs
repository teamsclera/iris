﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialgoueController : MonoBehaviour {

    [SerializeField]
    Text textbox;

    [SerializeField]
    Text talkingChar;

    string toWrite;

    public bool writingText;

    [SerializeField]
    float letterInterval;
    float currInterval;


    int currChar;

    [SerializeField]
    float fadeSpeed;
    bool fadingIn;

    [SerializeField]
    Image textBoxImage;

    [SerializeField]
    Sprite leftCharTextbox;

    [SerializeField]
    Sprite rightCharTextbox;

    [SerializeField]
    Sprite noCharTextbox;

    [HideInInspector]
    public float deadTime;

    void Start()
    {
        ToggleDialogue(false);
    }

    void Update()
    {
        if (writingText == true && currInterval <= 0)
        {
            WriteLetter();
        }
        else
        {
            currInterval -= Time.deltaTime;
        }

        if(fadingIn == true)
        {
            if(textBoxImage.color.a < 1)
            {
                textBoxImage.color = new Color(1, 1, 1, textBoxImage.color.a + fadeSpeed * Time.deltaTime);
            }
            else
            {
                fadingIn = false;
            }
        }

        if(writingText == false)
        {
            deadTime += Time.deltaTime;
        }

    }

    public void WriteDialogue(Dialogue targetDialogue)
    {
        currChar = 0;
        toWrite = targetDialogue.text;

        textBoxImage.sprite = noCharTextbox;

        writingText = true;

        deadTime = 0;

        if(targetDialogue.speakingChar != "None")
        {
            talkingChar.text = targetDialogue.speakingChar;
            textbox.fontStyle = FontStyle.Normal;

            textbox.alignment = TextAnchor.MiddleLeft;

            if (targetDialogue.speakingSide == "Left")
            {
                talkingChar.alignment = TextAnchor.MiddleLeft;
                textBoxImage.sprite = leftCharTextbox;
            }
            else if (targetDialogue.speakingSide == "Right")
            {
                talkingChar.alignment = TextAnchor.MiddleRight;
                textBoxImage.sprite = rightCharTextbox;
            }
        }
        else
        {
            talkingChar.text = "";
            textbox.fontStyle = FontStyle.Italic;
            textbox.alignment = TextAnchor.MiddleCenter;
        }

        AudioController.PlayAudio(targetDialogue.audioID, false);


        textbox.text = "";

    }

    public void WriteText(string _targetText)
    {
        currChar = 0;
        toWrite = _targetText;
        writingText = true;

        deadTime = 0;

    }

    public void EndDialogue()
    {
        toWrite = "";
        ClearDialogue();
    }

    public void ClearDialogue()
    {
        writingText = false;
        textBoxImage.sprite = noCharTextbox;
        textbox.text = "";
    }


    void WriteLetter()
    {
        char newChar = toWrite[currChar];

        currChar++;

        textbox.text = textbox.text + newChar;

        if (textbox.text.Length >= toWrite.Length)
        {
            FinishWriting();
        }

        currInterval = letterInterval;
    }

    public void CompleteDialogue()
    {
        writingText = false;
        SceneController.expressions.SetExpressions();
        
        textbox.text = toWrite;
    }

    void FinishWriting()
    {
        writingText = false;
    }

    public void ToggleDialogue(bool _toggle)
    {
        textBoxImage.gameObject.SetActive(_toggle);
        ClearDialogue();

        if (_toggle == true)
        {
            fadingIn = true;
            textbox.text = "";
        }
        else
        {
            textbox.text = "";
            textBoxImage.color = new Color(1, 1, 1, 0);
            fadingIn = false;
        }

    }

}
