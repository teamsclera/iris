﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExpressionSwitcher : MonoBehaviour
{

    [SerializeField]
    Image leftExpression;

    [SerializeField]
    Image rightExpression;

    [SerializeField]
    Image nextLeftExpression;

    [SerializeField]
    Image nextRightExpression;

    public Dictionary<string, Sprite> expressionDict = new Dictionary<string, Sprite>();

    [SerializeField]
    float fadeSpeed;

    Sprite leftExpTarget;
    Sprite rightExpTarget;

    ExpressionAction leftAction;
    ExpressionAction rightAction;

    bool leftChanging = false;
    bool rightChanging = false;

    // Use this for initialization
    void Start()
    {
        ClearExpressions();
    }

    // Update is called once per frame
    void Update()
    {
        if (leftChanging == true)
        {
            switch (leftAction)
            {
                case ExpressionAction.Clear:
                    FadeOut(leftExpression);


                    if (leftExpression.color.a <= 0)
                    {
                        leftExpression.color = new Color(1, 1, 1, 0);
                        leftChanging = false;
                    }
                    break;

                case ExpressionAction.Swap:
                    SwapExpression(leftExpression, nextLeftExpression, true);
                    break;

                default:
                    //Debug.Log("Left Done");
                    leftChanging = false;
                    break;
            }
        }

        if (rightChanging == true)
        {
            switch (rightAction)
            {
                case ExpressionAction.Clear:
                    FadeOut(rightExpression);

                    if (rightExpression.color.a <= 0)
                    {
                        rightExpression.color = new Color(1, 1, 1, 0);
                        rightChanging = false;
                    }
                    break;

                case ExpressionAction.Swap:
                    SwapExpression(rightExpression, nextRightExpression, false);
                    break;

                default:
                    //Debug.Log("Right Done");
                    rightChanging = false;
                    break;
            }
        }

    }

    void FadeIn(Image imageTarget)
    {
        float fadelevel = imageTarget.color.a + (fadeSpeed * Time.deltaTime * 1.25f);

        imageTarget.color = new Color(1, 1, 1, fadelevel);
    }

    void FadeOut(Image imageTarget)
    {
        float fadelevel = imageTarget.color.a - fadeSpeed * Time.deltaTime;

        imageTarget.color = new Color(1, 1, 1, fadelevel);
    }

    void SwapExpression(Image currImage, Image nextImage, bool expressionSide)
    {
        FadeOut(currImage);
        FadeIn(nextImage);


        if (nextImage.color.a >= 1)//when the fading is complete
        {
            currImage.sprite = nextImage.sprite;//assign next image to currImage

            if (expressionSide == true)//left
            {
                leftChanging = false;
            }
            else
            {
                rightChanging = false;
            }

            currImage.color = new Color(1, 1, 1, 1);
            nextImage.color = new Color(1, 1, 1, 0);
        }
    }

    public void ChangeExpressions(string _leftKey, string _rightKey)
    {
       // Debug.Log(_leftKey + "   " + _rightKey);

        //SetExpressions();//ensure the last expresseions are set correctly

        if (_leftKey != "None" && _leftKey != "none")
        {
            leftExpTarget = GetExpression(_leftKey);

            nextLeftExpression.sprite = leftExpTarget;

            leftAction = ExpressionAction.Swap;

            if (leftExpression.sprite != null && _leftKey == leftExpression.sprite.name) { leftAction = ExpressionAction.None; }
        }
        else { leftAction = ExpressionAction.Clear; leftExpTarget = null; }




        if (_rightKey != "None" && _rightKey != "none")
        {
            rightExpTarget = GetExpression(_rightKey);

            nextRightExpression.sprite = rightExpTarget;

            rightAction = ExpressionAction.Swap;

            if (rightExpression.sprite != null && _rightKey == rightExpression.sprite.name) { rightAction = ExpressionAction.None; }
        }
        else { rightAction = ExpressionAction.Clear; rightExpTarget = null; }


        leftChanging = true;
        rightChanging = true;
    }

    public void ClearExpressions()
    {
        leftExpression.sprite = null;
        rightExpression.sprite = null;

        nextLeftExpression.sprite = null;
        nextRightExpression.sprite = null;

        leftExpression.color = new Color(1, 1, 1, 0);
        rightExpression.color = new Color(1, 1, 1, 0);
        nextLeftExpression.color = new Color(1, 1, 1, 0);
        nextRightExpression.color = new Color(1, 1, 1, 0);

        //Debug.Log("Cleared Expressions");
    }

    public void SetExpressions()//set the expressions without transitions
    {
        if(leftExpTarget != null)
        {
            leftExpression.sprite = leftExpTarget;
            leftExpression.color = new Color(1, 1, 1, 1);
        }
        else
        {
            leftExpression.sprite = null;
            leftExpression.color = new Color(1, 1, 1, 0);
        }

        if (rightExpTarget != null)
        {
            rightExpression.sprite = rightExpTarget;
            rightExpression.color = new Color(1, 1, 1, 1);
        }
        else
        {
            rightExpression.sprite = null;
            rightExpression.color = new Color(1, 1, 1, 0);
        }

        nextLeftExpression.sprite = null;
        nextRightExpression.sprite = null;

        nextLeftExpression.color = new Color(1, 1, 1, 0);
        nextRightExpression.color = new Color(1, 1, 1, 0);

        leftChanging = false;
        rightChanging = false;
    }

    public void LoadExpressions()
    {

        Sprite[] foundExpressions = Resources.LoadAll<Sprite>("Expressions");

        foreach (Sprite expression in foundExpressions)
        {
            //Debug.Log(expression.name);
            expressionDict.Add(expression.name, expression);
        }

    }

    Sprite GetExpression(string expressionTarget)
    {
        Sprite foundExpression = expressionDict["Default"];

        if (expressionDict.ContainsKey(expressionTarget))
        {
            foundExpression = expressionDict[expressionTarget];
        }
        else
        {
            Debug.Log("Invalid Expression Key: " + expressionTarget);
        }

        return foundExpression;
    }
}


enum ExpressionAction
{
    None,
    Swap,
    Clear,
}