﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoPlayer : MonoBehaviour {

    [SerializeField]
    float playSpeed;

    [SerializeField]
    GameObject activeIndicator;

    public bool autoPlaying = true;

	// Use this for initialization
	void Start () {
        ToggleAutoplay(false);

    }
	
	// Update is called once per frame
	void Update () {

        if(SceneController.dialogue.deadTime > playSpeed && autoPlaying == true && SceneController.storySceneActive == true)
        {
            SceneController.NextDialogue();
        }
		
	}

    public void ToggleAutoplay(bool toggle)
    {
        autoPlaying = toggle;
        SceneController.dialogue.deadTime = 0;
        activeIndicator.SetActive(toggle);
    }
}
