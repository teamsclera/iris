﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour {

    static public  Dictionary<string, AudioClip> audioDict = new Dictionary<string, AudioClip>();

    [SerializeField]
    static AudioSource clipPlayer;

    [SerializeField]
    static AudioSource loopingPlayer;

    void Awake()
    {
        clipPlayer = GetComponent<AudioSource>();

        loopingPlayer = new GameObject("Looping Player").AddComponent<AudioSource>();

    }

    // Use this for initialization
    void Start () {


        loopingPlayer.transform.parent = this.transform;
        loopingPlayer.loop = true;


    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public static void LoadAudio()
    {
        Object[] foundAudio = Resources.LoadAll("Audio");

        foreach (Object audioObj in foundAudio)
        {
            if (audioDict.ContainsKey(audioObj.name) == false)
            {
                //Debug.Log(audioObj.name);
                audioDict.Add(audioObj.name, (AudioClip)audioObj);
            }

        }
    }

    public static void PlayAudio(string audioKey, bool looping)
    {


        if (audioKey != "None" && audioDict.ContainsKey(audioKey))
        {
            if(looping == true)
            {
                loopingPlayer.clip = audioDict[audioKey];
                loopingPlayer.Play();
                //Debug.Log("Now Looping Audio: " + audioKey);
            }
            else
            {
                clipPlayer.PlayOneShot(audioDict[audioKey]);
                //Debug.Log("Now Playing Audio: " + audioKey);
            }
        }
        else
        {
            if(audioKey != "None")
            {
                Debug.Log("Invalid audio key: " + audioKey);
            }


        }
    }

    public static void SetVolume(float _volume)
    {
        clipPlayer.volume = _volume;
        loopingPlayer.volume = _volume;
    }

    public static void ScaleVolume(float _volumeChange, bool _direction)
    {
        if(_direction == true)//up
        {
            clipPlayer.volume += _volumeChange;
            loopingPlayer.volume += _volumeChange;
        }
        else//down
        {
            clipPlayer.volume -= _volumeChange;
            loopingPlayer.volume -= _volumeChange;
        }
    }

    public static void StopAudio()
    {
        clipPlayer.clip = null;
        clipPlayer.Stop();

        loopingPlayer.clip = null;
        loopingPlayer.Stop();
    }
}
