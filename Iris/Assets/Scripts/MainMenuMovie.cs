﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuMovie : MonoBehaviour {

    [SerializeField]
    MovieTexture mainMenuMovie;

    Renderer movieRend;

    // Use this for initialization
    void Start () {
        mainMenuMovie.loop = true;

        //Play main menu movie image
        movieRend = GetComponent<Renderer>();
        movieRend.material.mainTexture = mainMenuMovie;
        mainMenuMovie.Play();
    }
	
	// Update is called once per frame
	void Update () {


		
	}
}
