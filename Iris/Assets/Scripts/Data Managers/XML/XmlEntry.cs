﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;


public class XmlEntry
{

    public string entryName;
    public Dictionary<string, XmlElementData> elements = new Dictionary<string, XmlElementData>();


    public XmlEntry(string _name, List<XmlElementData> _values)
	{
        entryName = _name;

        foreach(XmlElementData element in _values)
        {
            elements.Add(element.name, element);
        }
    }

    public string GetEntryData(string request)
    {
        return elements[request].value;
    }

    public List<XmlElementData> GetAllElements()
    {
        return elements.Values.ToList<XmlElementData>();
    }

}

public class XmlElementData
{
    public string name;
    public string value;

    public XmlElementData(string _name, string _value)
    {
        name = _name;
        value = _value;
    }
}

