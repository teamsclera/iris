﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;

public static class CSVReader  {


    public static List<string> ReadCsv(string filePath)
    {
        return GetCSVData(File.ReadAllText(filePath));
    }

    public static List<string> GetCSVData(string rawTextData)
    {
        string[] csvData = rawTextData.Split(new char[] { ',', '\n' }, System.StringSplitOptions.RemoveEmptyEntries);



        List<string> data = new List<string>();


        foreach (string value in csvData)
        {
            data.Add(value);
        }

        return data;
    }

    public static List<string> GetTSVData(string rawTextData)
    {
        string[] csvData = rawTextData.Split(new char[] { '\t', '\n' }, System.StringSplitOptions.RemoveEmptyEntries);



        List <string> data = new List<string>();


        foreach (string value in csvData)
        {
            //Debug.Log(value);
            data.Add(value);
        }

        return data;
    }
}
