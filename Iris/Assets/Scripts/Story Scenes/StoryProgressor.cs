﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class StoryProgressor{

    static List<Scene> scenes = new List<Scene>();

    public static int currScene = 0;

    static SceneTransitioner transitoner;

    public static bool skippingCombat;

    // Use this for initialization
    public static void StartProgressor(GameStory story)
    {
        scenes = story.scenes;

        currScene = 0;

        transitoner = GameObject.FindObjectOfType<SceneTransitioner>();
        
    }

    public static void NextScene()
    {

        currScene++;

        if(currScene >= scenes.Count)
        {
            GameObject.FindObjectOfType<GameController>().StartCredits();
        }
        else
        {
            if(skippingCombat == true && scenes[currScene].combatScene == true)
            {
                Debug.Log("Skipping Combat Scene Scene: " + currScene);
                NextScene();
            }
            else
            {

                Debug.Log("Starting Scene: " + currScene);

                transitoner.gameObject.SetActive(true);


                transitoner.TransitionScene(scenes[currScene]);

                GameObject.FindObjectOfType<GameController>().SaveGame();
            }


        }

    }

    public static void RepeatLastScene()
    {
        StartScene(currScene);
    }

    public static void SkipScene()
    {
        SceneController.storySceneActive = false;
        SceneController.combatSceneActive = false;

        NextScene();
    }

    public static void StartScene(int _sceneTarget)
    {
        transitoner.gameObject.SetActive(true);

        currScene = _sceneTarget;
        transitoner.TransitionScene(scenes[_sceneTarget]);
    }
}
