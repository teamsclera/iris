﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class GameStory {

    public List<Scene> scenes = new List<Scene>();

    

	public GameStory(List <string> rawStoryData)
    {
        //LoadTestData();

        scenes = new List<Scene>();

        int loadedConvos = 0;
        int loadedCombats = 0;

        foreach (string rawScene in rawStoryData)
        {
            List<string> rawSceneData = CSVReader.GetTSVData(rawScene);

            if (rawSceneData[0] == "Combat")
            {
                CreateCombatScene(rawSceneData);
                loadedCombats++;
            }
            else
            {
                CreateStoryScene(rawSceneData);
                loadedConvos++;
            }

            
        }

        //Debug.Log("Loaded " + loadedConvos + " story scenes and " + loadedCombats + " combat scenes");
        



    }

    void CreateStoryScene(List<string> rawSceneData)
    {
        List<Dialogue> sceneDialogue = new List<Dialogue>();

        string backdropKey = rawSceneData[0];
        string musicKey = rawSceneData[1];

        rawSceneData.RemoveRange(0,3);//remove backdrop,music and  weird empty values SOLVE THIS

        string leftExpressionKey = "";
        string rightExpressionKey = "";
        string speakingChar = "";
        string speakingSide = "";
        string text = "";
        string audioID = "";

        Dialogue newDiag;

        for(int cycleI = 0; rawSceneData.Count != 0; cycleI++)
        {
            //Debug.Log(rawSceneData[0]);

            switch (cycleI)
            {
                case 0://left key
                    leftExpressionKey = rawSceneData[0];
                    break;

                case 1://right key
                    rightExpressionKey = rawSceneData[0];
                    break;

                case 2://speaking
                    speakingChar = rawSceneData[0];
                    break;

                case 3://speaking side
                    speakingSide = rawSceneData[0];
                    break;

                case 4://text

                    text = rawSceneData[0];
                    break;

                case 5://audio

                    //remove the TSV end line character
                    List<char> stringChars = rawSceneData[0].ToCharArray().ToList();
                    stringChars.RemoveAt(stringChars.Count - 1);

                    audioID = new string(stringChars.ToArray());

                    //Debug.Log(audioID + " " + audioID.Length);

                    newDiag = new Dialogue(leftExpressionKey, rightExpressionKey, speakingChar, speakingSide,  text, audioID);

                    sceneDialogue.Add(newDiag);

                    cycleI = -1;
                    break;

            }

            rawSceneData.RemoveAt(0);//remove last entry
        }

        scenes.Add(new StoryScene(sceneDialogue, backdropKey, musicKey));
    }

    void CreateCombatScene(List<string> rawSceneData)
    {
        string backdropKey = rawSceneData[1];
        string musicKey = rawSceneData[2];
        string opponentID = rawSceneData[3];

        scenes.Add(new CombatScene(backdropKey, opponentID, musicKey));

    }

}
