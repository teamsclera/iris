﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SceneController {

    static public DialgoueController dialogue = GameObject.FindObjectOfType<DialgoueController>();
    static public BackdropSwitcher backdrop = GameObject.FindObjectOfType<BackdropSwitcher>();
    static public ExpressionSwitcher expressions = GameObject.FindObjectOfType<ExpressionSwitcher>();

    static List<Dialogue> sceneDialogue = new List<Dialogue>();

    static int currDialogue;

    public static bool storySceneActive = false;
    public static bool combatSceneActive = false;

    static public void StartStoryScene(StoryScene _sceneTarget)
    {
        backdrop.ChangeBackdrop(_sceneTarget.backdropID);

        currDialogue = 0;

        sceneDialogue = _sceneTarget.dialogue;

        storySceneActive = true;

        StartDialogue();
    }


    static public void EndScene()
    {
        Debug.Log("Ending Scene");
        storySceneActive = false;
        combatSceneActive = false;

        StoryProgressor.NextScene();
    }

    static public void NextDialogue()
    {
        currDialogue++;

        if(currDialogue >= sceneDialogue.Count)
        {
            EndScene();
        }
        else
        {
            GameObject.FindObjectOfType<ExpressionSwitcher>().ChangeExpressions(sceneDialogue[currDialogue].leftExpressionKey, sceneDialogue[currDialogue].rightExpressionKey);
            StartWriting();
        }
        
    }

    static public void StartDialogue()
    {
        dialogue.ToggleDialogue(true);
        GameObject.FindObjectOfType<ExpressionSwitcher>().ChangeExpressions(sceneDialogue[currDialogue].leftExpressionKey, sceneDialogue[currDialogue].rightExpressionKey);
        StartWriting();
    }

    static void StartWriting()
    {
        dialogue.WriteDialogue(sceneDialogue[currDialogue]);
    }
}
