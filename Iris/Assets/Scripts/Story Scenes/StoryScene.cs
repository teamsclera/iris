﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoryScene : Scene{

    public List<Dialogue> dialogue = new List<Dialogue>();

	public StoryScene(List<Dialogue> _dialogue, string _backdrop, string _musicKey)
    {
        dialogue = _dialogue;
        backdropID = _backdrop;
        musicKey = _musicKey;
        combatScene = false;
    }

}
