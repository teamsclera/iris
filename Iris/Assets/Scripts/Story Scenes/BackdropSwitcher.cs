﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BackdropSwitcher : MonoBehaviour {

    public Dictionary<string, Sprite> backdropDict = new Dictionary<string, Sprite>();

    SpriteRenderer backdropRend;

    void Start()
    {
        backdropRend = GetComponent<SpriteRenderer>();


    }

    public void ChangeBackdrop(string _backdropID)
    {
        backdropRend.sprite = backdropDict["Default"];

        if (backdropDict.ContainsKey(_backdropID))
        {
            backdropRend.sprite = backdropDict[_backdropID];
        }
        else
        {
            Debug.Log("Invalid Backdrop Key: " + _backdropID);

        }
    }

    public void LoadBackdrops()
    {
        Sprite[] foundBackdrops = Resources.LoadAll<Sprite>("Backgrounds");

        foreach (Sprite backdrop in foundBackdrops)
        {
            //Debug.Log(backdrop.name);
            backdropDict.Add(backdrop.name, backdrop);
        }
    }

}
