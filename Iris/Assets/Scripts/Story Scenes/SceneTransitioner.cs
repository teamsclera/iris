﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class SceneTransitioner : MonoBehaviour {

    bool transitioning;

    bool direction;

    Scene nextScene;

    float transparency;

    [SerializeField]
    float transitionSpeed;

    [SerializeField]
    GameObject mainMenuObj;

    Image blockerImage;

    CombatController combat;
    AutoPlayer autoPlayer;

    void Start()
    {
        blockerImage = GetComponent<Image>();
        combat = FindObjectOfType<CombatController>();
        autoPlayer = FindObjectOfType<AutoPlayer>();

    }

    // Update is called once per frame
    void Update () {
		
        if(transitioning == true)
        {
            if(direction == true)//fade up
            {
                transparency += transitionSpeed * Time.deltaTime;
                AudioController.ScaleVolume(transitionSpeed * Time.deltaTime, false);//lower volume
            }
            else//fade down
            {
                transparency -= transitionSpeed * Time.deltaTime;
                AudioController.ScaleVolume(transitionSpeed * Time.deltaTime, true);//raise volume
            }

            

            if (transparency > 1.1)//screen is covered at this point change scene here
            {
                transparency = 1;
                direction = false;
                mainMenuObj.SetActive(false);



                combat.ClearCombat();
                SceneController.dialogue.CompleteDialogue();

                SceneController.dialogue.ClearDialogue();
                SceneController.expressions.ClearExpressions();

                AudioController.StopAudio();//stop previouse audio


                if(FindObjectOfType<GameController>().menuActive == true)
                {
                    FindObjectOfType<GameController>().HideMainMenu();
                }

//START THE NEXT SCENE
                AudioController.PlayAudio(nextScene.musicKey, true);

                if (nextScene.combatScene == true)
                {
                    combat.StartCombat((CombatScene)nextScene);
                }
                else
                {
                    SceneController.StartStoryScene((StoryScene)nextScene);
                }
            }
            else if( transparency < 0)
            {
                EndTransition();
            }

            blockerImage.color = new Color(0, 0, 0, transparency);

        }

	}

    public void TransitionScene(Scene _nextScene)
    {
        nextScene = _nextScene;


        StartTransition();

        autoPlayer.ToggleAutoplay(false);

    }

    void StartTransition()
    {
        transitioning = true;
        direction = true;

        transparency = 0;
    }

    void EndTransition()
    {
        transitioning = false;

        transparency = 0;

        this.gameObject.SetActive(false);


    }
}
