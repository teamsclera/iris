﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XInputDotNetPure;

public static class ControllerInput {

    static GamePadState controllerState;

    public static bool hasController;

    public static void GetController()
    {
        controllerState = GetControllerState();

        if (controllerState.IsConnected)
        {
            hasController = true;
            Debug.Log("Controller is connected");
        }
        else
        {
            hasController = false;
            Debug.Log("No Controller Detected");
        }
    }

    public static GamePadState GetControllerState()
    {
        if(hasController == true)
        {
            controllerState = GamePad.GetState(PlayerIndex.One);
        }
        else
        {
            controllerState = new GamePadState();
        }

        return controllerState;
    }
}
