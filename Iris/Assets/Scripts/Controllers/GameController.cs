﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

    public List<TextAsset> gameProgression;


    [SerializeField]
    int startLocation;//the index position from where the story shall begin

    [SerializeField]
    GameObject mainMenuScreen;

    [SerializeField]
    GameObject startGameBtn;

    [SerializeField]
    GameObject continueGameBtn;

    [SerializeField]
    GameObject instructionGameBtn;

    [SerializeField]
    GameObject quitGameBtn;

    [SerializeField]
    GameObject normalGameBtn;

    [SerializeField]
    GameObject peacefulGameBtn;

    [SerializeField]
    GameObject loadingScreen;

    [SerializeField]
    GameObject creditsScreen;

    [SerializeField]
    UnityEngine.UI.RawImage howToPlayRend;

    [SerializeField]
    List<Texture2D> howToPlayScreens;
    int currInstructionScreen;

    bool gameStarted = false;

    bool gameLaunched = false;

    bool dataLoaded = false;

    bool loading = false;


    public bool howToPlayActive;
    public bool menuActive = false;
    public bool creditsActive = false;


    void Awake()
    {
        ControllerInput.GetController();

        loadingScreen.SetActive(true);
    }

	// Use this for initialization
	void Start () {

        LoadData();
    }
	
	// Update is called once per frame
	void Update () {

        PlayerController.PlayerUpdate();

        if (gameStarted == false && ControllerInput.hasController == true && ControllerInput.GetControllerState().Buttons.Start == XInputDotNetPure.ButtonState.Pressed)
        {
            StartGame(false);
        }
		

        if(gameLaunched == false && dataLoaded == true)
        {
            StartMainMenu();
            gameLaunched = true;

            Debug.Log("Game Ready");
        }

	}


    public void SaveGame()
    {
        //Debug.Log("Saved Game: " + StoryProgressor.currScene);

        CSVWriter.WriteToCsv(Application.persistentDataPath + "/IrisSaveData", StoryProgressor.currScene.ToString());
    }

    void LoadData()
    {
        AudioController.LoadAudio();
        FindObjectOfType<ExpressionSwitcher>().LoadExpressions();
        FindObjectOfType<BackdropSwitcher>().LoadBackdrops();

        GameStory story = new GameStory(LoadStoryData());

        StoryProgressor.StartProgressor(story);


        Debug.Log("Loaded: " + AudioController.audioDict.Count + " Audio" + " ::: "
            + FindObjectOfType<ExpressionSwitcher>().expressionDict.Count + " Expressions" + " ::: "
            + FindObjectOfType<BackdropSwitcher>().backdropDict.Count + " Backdrops" + " ::: "
            + story.scenes.Count + " Story Scenes");


        dataLoaded = true;
    }

    public void StartMainMenu()
    {
        gameStarted = false;
        creditsActive = false;

        creditsScreen.SetActive(false);
        menuActive = true;

        howToPlayRend.gameObject.SetActive(false);
        loadingScreen.SetActive(false);
        mainMenuScreen.SetActive(true);
    



        startGameBtn.SetActive(true);
        continueGameBtn.SetActive(System.IO.File.Exists(Application.persistentDataPath + "/IrisSaveData"));//show or hide continue button depending on if save data excists
        quitGameBtn.SetActive(true);
        instructionGameBtn.SetActive(true);
        normalGameBtn.SetActive(false);
        peacefulGameBtn.SetActive(false);



        AudioController.PlayAudio("Intro Theme Idea", true);


        if(GameObject.FindObjectOfType<SceneTransitioner>() == true)
        {
            GameObject.FindObjectOfType<SceneTransitioner>().gameObject.SetActive(false);
        }



    }

    public void StartHowToPlay()
    {
        howToPlayRend.gameObject.SetActive(true);
        howToPlayActive = true;
        currInstructionScreen = -1;

        HideMainMenu();

        NextHowToPlay();
    }

    public void SetupGame()
    {
        startGameBtn.SetActive(false);
        continueGameBtn.SetActive(false);
        quitGameBtn.SetActive(false);
        instructionGameBtn.SetActive(false);

        normalGameBtn.SetActive(true);
        peacefulGameBtn.SetActive(true);

        AudioController.PlayAudio("UI Click 1", false);

    }

    public void NextHowToPlay()
    {
        AudioController.PlayAudio("UI Click 1", false);

        currInstructionScreen++;

        if(currInstructionScreen >= howToPlayScreens.Count)
        {
            howToPlayActive = false;
            StartMainMenu();
        }
        else
        {
            howToPlayRend.texture = (Texture2D)howToPlayScreens[currInstructionScreen];

        }
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void HideMainMenu()
    {
        menuActive = false;


        mainMenuScreen.SetActive(false);
        startGameBtn.SetActive(false);
        continueGameBtn.SetActive(false);
        quitGameBtn.SetActive(false);
        instructionGameBtn.SetActive(false);

        normalGameBtn.SetActive(false);
        peacefulGameBtn.SetActive(false);


    }

    public void StartGame(bool skipCombat)
    {

        StoryProgressor.skippingCombat = skipCombat;

        Debug.Log("Starting New Game");

        AudioController.PlayAudio("UI Click 1", false);

        StoryProgressor.StartScene(startLocation);

        gameStarted = true;
    }


    public void LoadGame()
    {

        AudioController.PlayAudio("UI Click 1", false);


        if (System.IO.File.Exists(Application.persistentDataPath + "/IrisSaveData"))
        {
            string loadedProgress = CSVReader.ReadCsv(Application.persistentDataPath + "/IrisSaveData")[0];

            Debug.Log("Loaded Game starting at: " + int.Parse(loadedProgress));

            StoryProgressor.skippingCombat = false;


            StoryProgressor.StartScene(int.Parse(loadedProgress));
        }
        else
        {
            Debug.Log("No save data found starting new game");

            StartGame(false);
        }

        //Debug.Log(loadedProgress);
    }

    public void DeleteSaveData()
    {
        if (System.IO.File.Exists(Application.persistentDataPath + "/IrisSaveData"))
        {
            System.IO.File.Delete(Application.persistentDataPath + "/IrisSaveData");
        }
    }

    public void StartCredits()
    {
        creditsScreen.SetActive(true);
        AudioController.PlayAudio("Credit Ending Idea", true);
        creditsActive = true;


        DeleteSaveData();

    }

    public List<string> LoadStoryData()
    {
        List<string> data = new List<string>();

        foreach (TextAsset txtAsset in gameProgression)
        {
            data.Add(txtAsset.text);
        }

        return data;
    }
}
