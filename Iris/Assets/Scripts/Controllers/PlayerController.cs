﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PlayerController {

    static XInputDotNetPure.GamePadState lastState;

	public static void PlayerUpdate () {


        if (Input.GetMouseButtonDown(0) == true && SceneController.storySceneActive == true || ControllerInput.GetControllerState().Buttons.A == XInputDotNetPure.ButtonState.Pressed && SceneController.storySceneActive && lastState.Buttons.A == XInputDotNetPure.ButtonState.Released)
        {
            if(SceneController.dialogue.writingText == true)
            {
                SceneController.expressions.SetExpressions();
                SceneController.dialogue.CompleteDialogue();
            }
            else if (SceneController.dialogue.deadTime > 0.2f)
            {
                SceneController.NextDialogue();
            }
        }
        else if(Input.GetMouseButtonDown(0) == true && GameObject.FindObjectOfType<GameController>().howToPlayActive == true)
        {
            GameObject.FindObjectOfType<GameController>().NextHowToPlay();
        }
        else if (Input.GetMouseButtonDown(0) == true && GameObject.FindObjectOfType<GameController>().creditsActive == true)
        {
            GameObject.FindObjectOfType<GameController>().StartMainMenu();
        }

        if (Input.GetKeyDown(KeyCode.Space) || ControllerInput.GetControllerState().Buttons.X == XInputDotNetPure.ButtonState.Pressed && lastState.Buttons.X == XInputDotNetPure.ButtonState.Released)
        {
            AutoPlayer auto = GameObject.FindObjectOfType<AutoPlayer>();

            if(auto.autoPlaying == true)
            {
                auto.ToggleAutoplay(false);
            }
            else
            {
                auto.ToggleAutoplay(true);
            }
        }

        if(Input.GetKeyDown(KeyCode.End))
        {
            StoryProgressor.SkipScene();
        }

        if (Input.GetKeyDown(KeyCode.Home))
        {
            StoryProgressor.RepeatLastScene();
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            GameController game = GameObject.FindObjectOfType<GameController>();

            if (game.menuActive == true)
            {
                game.QuitGame();
            }
            else
            {
                game.StartMainMenu();
            }
        }

        lastState = ControllerInput.GetControllerState();


    }
}
