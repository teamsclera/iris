﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Combatant : MonoBehaviour {

    [SerializeField]
    List<Sprite> idleSprites;

    [SerializeField]
    List<Sprite> moveSprites;
    int currSprite;

    [SerializeField]
    List<Sprite> attackSprites;

    [SerializeField]
    List<Sprite> hitSprites;

    [SerializeField]
    SpriteRenderer characterRend;

    [SerializeField]
    SpriteRenderer symbolRend;

    public float fieldPosition;

    [HideInInspector]
    public bool stepComplete;
    [HideInInspector]
    public bool actionComplete;

    public CombatantOrientation currOrientation;
    public CombatantState currState;
    public TurnAction currAction;

    [HideInInspector]
    public Slider slider;

    CombatController combat;

    float spriteChangeSpeed = 0.08f;//the time between sprite changes
    float currSpriteTime;

    float remainingJourney;

    public float powerPool;

    public float attackPowerMultiplier;

    public float moveDistMultiplier;

    public float damageMultiplier;

    void Awake()
    {
        combat = FindObjectOfType<CombatController>();

        characterRend.sprite = idleSprites[Random.Range(0, idleSprites.Count - 1)];
    }

    public void StartCombatant(Sprite symbolSprite)
    {
        symbolRend.sprite = symbolSprite;
        StartIdle();
    }

    public void MoveUpdate () {

        currState = CombatantState.Moving;
        ChangeSprite();

    }

    public void IdleUpdate()
    {
        currState = CombatantState.Idling;
    }

    public void AttackUpdate()
    {
        currState = CombatantState.Attacking;
        ChangeSprite();

        if(currSprite >= attackSprites.Count)
        {
            StartIdle();
        }
    }

    public void HitUpdate()
    {
        currState = CombatantState.Hit;
    }

    public void ChangeSprite()
    {

        if (currSpriteTime <= 0)
        {
            currSpriteTime = spriteChangeSpeed;

            switch (currState)
            {
                case CombatantState.Moving:

                    if (currSprite >= moveSprites.Count)
                    {
                        currSprite = 0;
                    }

                    characterRend.sprite = moveSprites[currSprite];
                    //AudioController.PlayAudio(combat.movementAudioKeys[Random.Range(0, combat.movementAudioKeys.Count - 1)], false);

                    currSprite++;




                    break;

                case CombatantState.Attacking:

                    if (currSprite >= attackSprites.Count)
                    {
                        currSprite = 0;
                    }

                    characterRend.sprite = attackSprites[currSprite];

                    currSprite++;



                    break;

                case CombatantState.Hit:
                    break;

                case CombatantState.Idling:
                    //rend.sprite = idleSprites[0];//[Random.Range(0, idleSprites.Count - 1)];
                    break;
            }
        }
        else
        {
            if(currState != CombatantState.Idling)
            {
                currSpriteTime -= Time.deltaTime;
            }
        }

        
    }

    public void StepComplete()
    {
        stepComplete = true;
    }


    public void StartMove()
    {
        currSprite = 0;
        currState = CombatantState.Moving;
    }

    public void StartAttack()
    {
        currSprite = 0;
        currState = CombatantState.Attacking;

        AudioController.PlayAudio(combat.attackAudioKeys[Random.Range(0, combat.attackAudioKeys.Count - 1)], false);
    }

    public void StartIdle()
    {
        currState = CombatantState.Idling;
        characterRend.sprite = idleSprites[Random.Range(0, idleSprites.Count - 1)];
    }

    public void RecieveAttack(float attackPower)
    {
        currState = CombatantState.Hit;
        characterRend.sprite = hitSprites[Random.Range(0, idleSprites.Count - 1)];

        AudioController.PlayAudio(combat.damageAudioKeys[Random.Range(0, combat.damageAudioKeys.Count - 1)], false);

        fieldPosition -= attackPower * damageMultiplier;

        if(fieldPosition <= 0)
        {
            fieldPosition = 0;
        }
    }

    public void ChangeOrientation(CombatantOrientation newOrientation)
    {
        if(newOrientation == CombatantOrientation.Left)
        {
            currOrientation = CombatantOrientation.Left;
            transform.rotation = new Quaternion(0, 0, 0, 0);
        }
        else
        {
            currOrientation = CombatantOrientation.Right;
            transform.rotation = new Quaternion(0, 180, 0, 0);
        }
    }
}

public enum CombatantOrientation
{
    Left,
    Right,
}

public enum CombatantState
{
    Moving,
    Idling,
    Attacking,
    Hit,
}

