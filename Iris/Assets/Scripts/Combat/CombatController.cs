﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class CombatController : MonoBehaviour
{

    DialgoueController dialouge;
    ExpressionSwitcher expression;
    Camera cam;

    public float combatantSpeed;

    [SerializeField]
    GameObject combatUI;

    bool turnActive = false;

    [SerializeField]
    GameObject combatantsContainer;

    [SerializeField]
    Dictionary<string,GameObject> combatants = new Dictionary<string, GameObject>();

    [SerializeField]
    Text powerTxt;

    [SerializeField]
    List<Button> actionOptionsBtns;

    [SerializeField]
    List<Slider> combatantSliders;

    [SerializeField]
    Slider powerSlider;

    [SerializeField]
    GameObject powerContainer;

    int currPower;

    public List<string> attackAudioKeys;

    public List<string> damageAudioKeys;

    public List<string> movementAudioKeys;

    int selectedAction;

    CombatScene currScene;

    [SerializeField]
    int powerGrowth;

    [SerializeField]
    int startingPower;

    [SerializeField]
    float attackDist;

    float currTurnTime;

    bool canAttack;

    Combatant player;
    Combatant opponent;

    Combatant activeCombatant;
    Combatant inactiveCombatant;

    bool playerTurnCompleted;

    bool attacked;

    Dictionary<string, Sprite> combatantSymbols = new Dictionary<string, Sprite>();



    float moveTime;



    // Use this for initialization
    void Start()
    {

        dialouge = FindObjectOfType<DialgoueController>();
        expression = FindObjectOfType<ExpressionSwitcher>();
        cam = FindObjectOfType<Camera>();

        combatUI.SetActive(false);

        foreach(Transform child in combatantsContainer.transform)
        {
            combatants.Add(child.name, child.gameObject);

            child.gameObject.SetActive(false);

        }

        player = combatants["Dox"].GetComponent<Combatant>();
        opponent = combatants["Default"].GetComponent<Combatant>();



        Sprite[] foundSymbols = Resources.LoadAll<Sprite>("Combat Sprites");

        foreach (Sprite symbol in foundSymbols)
        {
            //Debug.Log(expression.name);
            combatantSymbols.Add(symbol.name, symbol);
        }

    }

    public void StartCombat(CombatScene _scene)
    {
        GameObject.FindObjectOfType<BackdropSwitcher>().ChangeBackdrop(_scene.backdropID);
        SceneController.combatSceneActive = true;

        currScene = _scene;

        combatUI.SetActive(true);
        dialouge.ToggleDialogue(false);

        powerTxt.gameObject.SetActive(true);
        powerContainer.gameObject.SetActive(true);

        player = combatants["Dox"].GetComponent<Combatant>();
        player.StartCombatant(new Sprite());
        player.slider = combatantSliders[0];


        if (combatants.ContainsKey(currScene.opponentID))
        {
            opponent = combatants[currScene.opponentID].GetComponent<Combatant>();
            opponent.StartCombatant(new Sprite());
        }
        else
        {
            opponent = combatants["Default"].GetComponent<Combatant>();

            if (currScene.opponentID != "None")
            {
                opponent.StartCombatant(combatantSymbols[currScene.opponentID]);
            }
            else
            {
                opponent.StartCombatant(new Sprite());
            }
        }

        opponent.slider = combatantSliders[1];


        player.gameObject.SetActive(true);
        opponent.gameObject.SetActive(true);


        player.fieldPosition = 30;
        opponent.fieldPosition = 30;

        selectedAction = 1234;

        combatantSliders[0].value = player.fieldPosition;
        combatantSliders[1].value = opponent.fieldPosition;

        player.StartIdle();
        opponent.StartIdle();



        //will have powergrowth added to it on first turn
        player.powerPool = startingPower;
        opponent.powerPool = startingPower;

        player.currAction = TurnAction.PowerUp;
        opponent.currAction = TurnAction.PowerUp;


        //player.StartCombatant(combatantSymbols["Dox"]);

        player.ChangeOrientation(CombatantOrientation.Left);
        opponent.ChangeOrientation(CombatantOrientation.Right);


        canAttack = false;

        StartTurn();
    }

    public void ClearCombat()
    {
        player.gameObject.SetActive(false);
        opponent.gameObject.SetActive(false);


        foreach (Button btn in actionOptionsBtns) { btn.gameObject.SetActive(false); }//hide btns
        powerTxt.gameObject.SetActive(false);
        powerContainer.gameObject.SetActive(false);

        combatUI.SetActive(false);
    }

    void ToggleUIIntractability(bool toggle)
    {
        foreach (Button btn in actionOptionsBtns) { btn.interactable = toggle; }

        Color fadeColour;

        for (int i = 0; i < actionOptionsBtns.Count; i++)
        {
            fadeColour = actionOptionsBtns[i].GetComponentInChildren<Text>().color;

            if (toggle == true)
            {
                fadeColour.a = 1;
            }
            else
            {
                fadeColour.a = 0.5f;
            }

            actionOptionsBtns[i].GetComponentInChildren<Text>().color = fadeColour;

            powerTxt.color = fadeColour;


            foreach(Image img in powerContainer.GetComponentsInChildren<Image>())
            {
                img.color = fadeColour;
            }

        }

        


        powerSlider.interactable = toggle;

    }

    void EndCombat(bool result)
    {
        //ClearCombat();
        SceneController.combatSceneActive = false;

        if (result == true)
        {
            StoryProgressor.NextScene();
            Debug.Log("Victory");
        }
        else
        {
            StoryProgressor.RepeatLastScene();
            Debug.Log("Defeat");
        }


    }

    // Update is called once per frame
    void Update()
    {


        if (turnActive == true)
        {
            currTurnTime += Time.deltaTime;


            switch (activeCombatant.currState)
            {
                case CombatantState.Moving:

                    if (Mathf.Round(activeCombatant.slider.value) != Mathf.Round(activeCombatant.fieldPosition))
                    {
                        activeCombatant.MoveUpdate();

                        MoveCombatant(activeCombatant, combatantSpeed);

                        //Debug.Log(Mathf.Round(activeCombatant.slider.value) + " ::: " + Mathf.Round(activeCombatant.fieldPosition));

                    }
                    else
                    {
                        activeCombatant.StartIdle();
                    }


                    break;

                case CombatantState.Attacking:
                    activeCombatant.AttackUpdate();
                    break;

                case CombatantState.Idling:
                    activeCombatant.IdleUpdate();

                    if(inactiveCombatant.currState != CombatantState.Hit && currTurnTime > 0.1)
                    {
                        EndTurn();
                    }

                    break;
            }


            if(inactiveCombatant.currState == CombatantState.Hit)
            {

                if (Mathf.Round(inactiveCombatant.slider.value) != Mathf.Round(inactiveCombatant.fieldPosition))
                {
                    inactiveCombatant.HitUpdate();
                    MoveCombatant(inactiveCombatant, combatantSpeed / 2);
                }
                else
                {
                    inactiveCombatant.StartIdle();
                }

            }

            if(currTurnTime > 2)
            {
                Debug.Log("Turn time expired forced new turn");
                EndTurn();
            }

        }
        

        }

    public void SelectAction(int _target)
    {
        SetActionText();

        

        if (selectedAction == _target)//if the target action has been selected a second time then trigger the turn
        {
            CommenceTurn(player);
            inactiveCombatant = opponent;

            AudioController.PlayAudio("UI Click 2", false);
        }
        else//a different action has been selected
        {
            if(_target == 0 && canAttack == false)
            {
                selectedAction = 1234;//dummy value
                //Debug.Log("Cannot attack");
            }
            else
            {
                AudioController.PlayAudio("UI Click 1", false);

                selectedAction = _target;
                actionOptionsBtns[_target].GetComponentInChildren<Text>().color = new Color(1, 1, 0);

                switch (selectedAction)
                {
                    case 0:
                        player.currAction = TurnAction.Attack;
                        break;

                    case 1:
                        player.currAction = TurnAction.PowerUp;
                        break;

                    case 2:
                        player.currAction = TurnAction.Forward;
                        break;

                    case 3:
                        player.currAction = TurnAction.Backward;
                        break;

                }
            }



        }


    }

    void CommenceTurn(Combatant turnCombatant)
    {
        //expression.ChangeExpressions(playerAction.expressionID, opponentAction.expressionID);

        activeCombatant = turnCombatant;


        if(activeCombatant == opponent)
        {
            inactiveCombatant = player;

            currPower = Mathf.RoundToInt(Random.Range(activeCombatant.powerPool / 2, activeCombatant.powerPool));

            if(Mathf.Abs(player.fieldPosition - (100 - opponent.fieldPosition)) <= attackDist)
            {
                opponent.currAction = TurnAction.Attack;
            }
            else
            {
                opponent.currAction = TurnAction.Forward;
            }

            if(activeCombatant.powerPool < 15 && opponent.currAction != TurnAction.Attack)
            {
                opponent.currAction = TurnAction.PowerUp;
            }
        }

        switch (turnCombatant.currAction)
        {
            case TurnAction.Forward:
                activeCombatant.fieldPosition += currPower * activeCombatant.moveDistMultiplier;

                if(activeCombatant.fieldPosition >= (100 - inactiveCombatant.fieldPosition))
                {
                    //Debug.Log("move out of bounds");
                    activeCombatant.fieldPosition = (100 - inactiveCombatant.fieldPosition) - 1;
                }

                activeCombatant.StartMove();
                moveTime = 0;


                activeCombatant.powerPool -= currPower;
                break;

            case TurnAction.Backward:
                activeCombatant.fieldPosition -= currPower * activeCombatant.moveDistMultiplier;

                activeCombatant.StartMove();
                moveTime = 0;

                activeCombatant.powerPool -= currPower;
                break;

            case TurnAction.Attack:
                activeCombatant.StartAttack();
                inactiveCombatant.RecieveAttack(currPower * activeCombatant.attackPowerMultiplier);
                moveTime = 0;


                activeCombatant.powerPool -= currPower;
                break;

            case TurnAction.PowerUp:
                activeCombatant.StartIdle();
                //EndTurn();
                break;
        }


        currTurnTime = 0;


        turnActive = true;
        attacked = false;

        //foreach (Button btn in actionOptionsBtns) { btn.gameObject.SetActive(false); }//hide btns
        //powerTxt.gameObject.SetActive(false);
        //powerContainer.gameObject.SetActive(false);

        ToggleUIIntractability(false);


    }

    void StartTurn()
    {
        player.powerPool += powerGrowth;
        opponent.powerPool += powerGrowth;

        canAttack = false;
        selectedAction = 1234;//dummy value

        activeCombatant = player;
        inactiveCombatant = opponent;

        SetActionText();

        ToggleUIIntractability(true);


        powerSlider.maxValue = player.powerPool;
        powerSlider.value = (player.powerPool / 2);
        currPower = Mathf.RoundToInt(powerSlider.value);

        ChangePowerSlider();

        SetCombatantPositions();




        if (Mathf.Abs(player.slider.value - (100 - opponent.slider.value)) <= attackDist)
        {
            //Debug.Log(player.slider.value + "   " + opponent.slider.value + "   " + Mathf.Abs(player.slider.value - (100 - opponent.slider.value)));

            canAttack = true;
            SetActionText();

        }



    }

    void SetActionText()
    {
        for(int i = 0; i < actionOptionsBtns.Count; i++)
        {
            actionOptionsBtns[i].gameObject.SetActive(true);
            actionOptionsBtns[i].GetComponentInChildren<Text>().color = new Color(1, 1, 1);
            //actionOptionsBtns[i].GetComponentInChildren<Text>().text = (i + 1) + ": " + currPlayerOptions[i].actionText;
            //actionOptionsBtns[i].GetComponentInChildren<Text>().text += " (Move: " + currPlayerOptions[i].movePower + ", Attack: " + currPlayerOptions[i].attackPower + ")";

            switch (i)
            {
                case 0://attack
                    actionOptionsBtns[i].GetComponentInChildren<Text>().text = "ATTACK";

                    if(canAttack == false)
                    {
                        actionOptionsBtns[i].GetComponentInChildren<Text>().color = new Color(0, 0, 0.07f, 1);
                    }
                    else
                    {
                        actionOptionsBtns[i].GetComponentInChildren<Text>().color = new Color(1, 1, 1, 1);
                    }
                    break;

                case 1://prepare
                    actionOptionsBtns[i].GetComponentInChildren<Text>().text = "POWER UP";//:\n+" + powerGrowth;
                    break;

                case 2://move forward
                    actionOptionsBtns[i].GetComponentInChildren<Text>().text = "FORWARD";
                    break;

                case 3://move back
                    actionOptionsBtns[i].GetComponentInChildren<Text>().text = "BACKWARD";
                    break;
            }

        }

        powerTxt.gameObject.SetActive(true);


    }

    void EndTurn()
    {
        turnActive = false;

        player.StartIdle();
        opponent.StartIdle();

        //Debug.Log("Turn Over");

        if (player.slider.value <= 5)
        {
            EndCombat(false);//defeat
        }
        else if (opponent.slider.value <= 5)
        {
            EndCombat(true);//victory
        }
        else
        {
            if (activeCombatant == player)
            {
                CommenceTurn(opponent);
                inactiveCombatant = player;

            }
            else
            {
                inactiveCombatant = opponent;
                StartTurn();
            }

        }




    }

    void SetCombatantPositions()
    {
        Vector2 playerPos = new Vector2(cam.ScreenToWorldPoint(combatantSliders[0].handleRect.transform.position).x, player.transform.position.y);
        player.transform.position = playerPos;

        Vector2 opponentPos = new Vector2(cam.ScreenToWorldPoint(combatantSliders[1].handleRect.transform.position).x, opponent.transform.position.y);
        opponent.transform.position = opponentPos;
    }

    void MoveCombatant(Combatant movingCombatant, float moveSpeed)
    {
        moveTime += moveSpeed * Time.deltaTime;
        movingCombatant.slider.value = Mathf.Lerp(movingCombatant.slider.value, movingCombatant.fieldPosition, moveTime);

        SetCombatantPositions();
    }

    public void ChangePowerSlider()
    {
        currPower = Mathf.RoundToInt(powerSlider.value);
        powerTxt.text = "POWER\n" + currPower + "/" + player.powerPool;
    }
}

public enum TurnAction
{
    Forward,
    Backward,

    Attack,
    PowerUp,
}