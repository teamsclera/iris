﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatAction : MonoBehaviour {

    public string expressionID;
    public string actionText;

    public string audioID;

    public int attackPower;
    public int movePower;

    public TurnAction action;

    public CombatAction(string _expressionID, string _text, string _audioKey, TurnAction _action)
    {
        expressionID = _expressionID;
        actionText = _text;
        audioID = _audioKey;
        action = _action;

    }
}


