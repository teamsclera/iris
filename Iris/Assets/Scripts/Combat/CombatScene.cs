﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatScene : Scene{

    public List<CombatDialogue> dialogue = new List<CombatDialogue>();

    public string opponentID;

    public CombatScene(string _backdrop, string _opponentID, string _musicKey)
    {
        opponentID = _opponentID;
        backdropID = _backdrop;
        musicKey = _musicKey;
        combatScene = true;
    }
}
